package com.adityafjr.materialtab.Fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.adityafjr.materialtab.R;


public class Tab1Fragment extends Fragment {
    private EditText edtPanjang, edtLebar;
    private Button btnHitung;
    private TextView txtLuas;
    public Tab1Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);

        edtLebar = (EditText) view.findViewById(R.id.edt_lebar);
        edtPanjang = (EditText) view.findViewById(R.id.edt_panjang);
        btnHitung = (Button) view.findViewById(R.id.btn_hitung);
        txtLuas = (TextView) view.findViewById(R.id.txt_luas);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        btnHitung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String panjang = edtPanjang.getText().toString().trim();
                String lebar = edtLebar.getText().toString().trim();

                try{
                    if (TextUtils.isEmpty(panjang) || TextUtils.isEmpty(lebar)){
                        Toast.makeText(getContext(),"Tidak boleh kosong",Toast.LENGTH_SHORT).show();
                    }else{
                        double p = Double.parseDouble(panjang);
                        double l = Double.parseDouble(lebar);

                        double luas = p * l;

                        txtLuas.setText("Luas : "+luas);
                    }
                }catch (NumberFormatException e){
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}


