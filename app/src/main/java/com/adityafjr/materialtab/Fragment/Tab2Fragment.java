package com.adityafjr.materialtab.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.adityafjr.materialtab.R;
import com.adityafjr.materialtab.Sub1Activity;
import com.adityafjr.materialtab.Sub2Activity;

public class Tab2Fragment extends Fragment {
    private Button btnSub1, btnSub2, btnDial;
    private String strIntent;
    private EditText txtIntent;

    public Tab2Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab2, container, false);

        btnSub1 = (Button) view.findViewById(R.id.btn_activity_sub_1);
        btnSub2 = (Button) view.findViewById(R.id.btn_activity_sub_2);
        btnDial = (Button) view.findViewById(R.id.btn_activity_dial);
        txtIntent = (EditText) view.findViewById(R.id.text_Intent);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnSub1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), Sub1Activity.class);
                startActivity(intent);
            }
        });

        btnSub2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strIntent = txtIntent.getText().toString();
                Intent intent = new Intent(getContext(), Sub2Activity.class);
                intent.putExtra(Sub2Activity.KEY_DATA, strIntent);
                startActivityForResult(intent, 0);
            }
        });

        btnDial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:082214709868"));
                startActivity(intent);
            }
        });
    }
}
